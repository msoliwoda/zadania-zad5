#!/usr/bin/python
# -*- coding: utf-8 -*-

from urlparse import parse_qs
import datetime
import cgi
import sys, os

database = {
    'id1': {'title': 'CherryPy Essentials: Rapid Python Web Application Development',
            'isbn': '978-1904811848',
            'publisher': 'Packt Publishing (March 31, 2007)',
            'author': 'Sylvain Hellegouarch',
            },
    'id2': {'title': 'Python for Software Design: How to Think Like a Computer Scientist',
            'isbn': '978-0521725965',
            'publisher': 'Cambridge University Press; 1 edition (March 16, 2009)',
            'author': 'Allen B. Downey',
            },
    'id3': {'title': 'Foundations of Python Network Programming',
            'isbn': '978-1430230038',
            'publisher': 'Apress; 2 edition (December 21, 2010)',
            'author': 'John Goerzen',
            },
    'id4': {'title': 'Python Cookbook, Second Edition',
            'isbn': '978-0-596-00797-3',
            'publisher': 'O''Reilly Media',
            'author': 'Alex Martelli, Anna Ravenscroft, David Ascher',
            },
    'id5': {'title': 'The Pragmatic Programmer: From Journeyman to Master',
            'isbn': '978-0201616224',
            'publisher': 'Addison-Wesley Professional (October 30, 1999)',
            'author': 'Andrew Hunt, David Thomas',
            },
    }


class BookDB():
    def titles(self):
        titles = [dict(id=id, title=database[id]['title']) for id in database.keys()]
        return titles

    def title_info(self, id):
        return database[id]


def application(environ, start_response):
    # Budowa ciała odpowiedzi
    response_body = """
<html>
    <head>
        <meta charset="utf-8" />
        <title>PD5: WSGI App</title>
    </head>
    <body>"""

    query = parse_qs(environ['QUERY_STRING'])
    id = query.get('id', [''])[0]
    baseurl='http://194.29.175.240/~p19/wsgi-bin/app.wsgi'

    # wyświetlenie listy książek z biblioteki
    if id == "":
        try:
            for book in BookDB().titles():
                response_body += '<div><a href="' + baseurl + '?id=' + book['id'] + '">' + book['title'] + '</a></div></br>'
            response_body += '</body></html>'
        except:
            status = "404 Not Found"
            response_body += "<u>404 ERROR</u>: Brak książek w bazie."
    # wyświetlenie danych o wybranej książce
    else:
        try:
            selected_book = BookDB().title_info(id)
            response_body += """
        <div>Book ID: %s</div></br>
        <div>Title: %s</div></br>
        <div>ISBN: %s</div></br>
        <div>Publisher: %s</div></br>
        <div>Author: %s</div></br>
    </body>
</html>
            """ % (
                id,
                selected_book['title'],
                selected_book['isbn'],
                selected_book['publisher'],
                selected_book['author']
            )
        except:
            status = "404 Not Found"
            response_body += "<u>404 ERROR</u>: Brak książki o danym id."


    # Status HTTP
    status = '200 OK'

    # Nagłówki HTTP oczekiwane przez klienta.
    response_headers = [('Content-Type', 'text/html; charset=utf-8'),
                        ('Content-Length', str(len(response_body)))]

    # Wysłanie ich do serwera przy użyciu dostarczonej metody (callback)
    start_response(status, response_headers)

    # Zwrot ciała odpowiedzi
    return [response_body]
